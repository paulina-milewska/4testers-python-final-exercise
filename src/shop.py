class Order:
    def __init__(self, customer_email):
        self.customer_email = customer_email
        self.products = []
        self.purchased = False

    def add_product(self, product):
        self.products.append(product)

    def get_total_price(self):
        total_price = 0
        for product in self.products:
            total_price += product.get_price()
        return total_price


    def get_total_quantity_of_products(self):
        total_quantity_of_products = 0
        for product in self.products:
             total_quantity_of_products += product.quantity
        return total_quantity_of_products


    def purchase(self):
        self.purchased = True


class Product:
    def __init__(self, name, unit_price, quantity=1):
        self.name = name
        self.unit_price = unit_price
        self.quantity = quantity

    def get_price(self):
        return self.quantity * self.unit_price


if __name__ == '__main__':
    product1 = Product('Shoes', 100.0)
    product2 = Product('Shoes', 30.0, 3.0)
    product3 = Product('T-shirt', 50.0, 2.0)
    product4 = Product('Bag', 10.0)

    order = Order('customer@example.com')
    order.add_product(product1)
    order.add_product(product2)
    order.add_product(product3)
    order.add_product(product4)

    print(order.get_total_price())
    print(order.get_total_quantity_of_products())
    order.purchase()
    print(order.purchased)
